int Taille = 5;
int Taille1 = 10;
int Allienbloc = Taille1 * 7 + 5;
Joueur joueur;
ArrayList alliens = new ArrayList();
ArrayList projectile = new ArrayList();
int direction = 1;
boolean incy = false;

void setup() {
    background(0);
    noStroke();
    fill(255);
    size(1000, 700);
    joueur = new Joueur();
    creationEnnemis();
}

void draw() {
    background(0);
    joueur.draw();

    for (int i = 0; i < projectile.size(); i++) {
        Projectile missile = (Projectile) projectile.get(i);
        missile.draw();
    }

    for (int i = 0; i < alliens.size(); i++) {
        Allien ennemi = (Allien) alliens.get(i);
        if (ennemi.dehors() == true) {
            direction *= (-1);
            incy = true;
            break;
        }
    }

    for (int i = 0; i < alliens.size(); i++) {
        Allien ennemi = (Allien) alliens.get(i);
        if (!ennemi.vivant()) {
            alliens.remove(i);
        } else {
            ennemi.draw();
        }
    }

    incy = false;
}

void creationEnnemis() {

    for (int i = 0; i < 9; i++) {
        for (int j = 0; j <= 4; j++) {
            int u = int(random(1, 5));
            if (u == 1) {
                alliens.add(new Allien(i * Allienbloc, j * Allienbloc, 240, 20, 20));
            } else if (u == 2) {
                alliens.add(new Allien(i * Allienbloc, j * Allienbloc, 240, 20, 200));
            } else if (u == 3) {
                alliens.add(new Allien(i * Allienbloc, j * Allienbloc, 20, 200, 20));
            } else if (u == 4) {
                alliens.add(new Allien(i * Allienbloc, j * Allienbloc, 20, 100, 250));
            } else if (u == 5) {
                alliens.add(new Allien(i * Allienbloc, j * Allienbloc, 240, 200, 0));
            } else {

                alliens.add(new Allien(i * Allienbloc, j * Allienbloc, 0, 0, 0));
            }
        }
    }
}

class PlateformeJoueur {
    int x, y;
    int coul1, coul2, coul3;
    String Vaisseau[];

    void draw() {
        Actualise();
        DessineVaisseau(x, y, coul1, coul2, coul3);
    }

    void DessineVaisseau(int Xposition, int Yposition, int coul1, int coul2, int coul3) {
        for (int i = 0; i < Vaisseau.length; i++) {
            String rang = (String) Vaisseau[i];

            for (int j = 0; j < rang.length(); j++) {
                if (rang.charAt(j) == '1')
                    fill(coul1, coul2, coul3);
                rect(Xposition + (j), Yposition + (i), 50, 50);
            }
        }
    }

    void Actualise() {
    }
}

class Joueur extends PlateformeJoueur {
    boolean Feu = true;
    int Recharge = 0;

    Joueur() {
        x = width / Allienbloc / 2;
        y = height - (10 * Taille);
        coul1 = 255;
        coul2 = 204;
        coul3 = 51;
        Vaisseau = new String[5];
        Vaisseau[0] = "0001000";
        Vaisseau[1] = "0111110";
        Vaisseau[2] = "0011100";
        Vaisseau[3] = "1111111";
        Vaisseau[4] = "0011100";
    }

    void Actualise() {
        if (keyPressed && keyCode == LEFT) x -= 5;
        if (keyPressed && keyCode == RIGHT) x += 5;
        if (keyPressed && key == ' ' && Feu) {
            projectile.add(new Projectile(x, y));
            Feu = false;
            Recharge = 0;
        }
        Recharge++;
        if (Recharge >= 20) {
            Feu = true;
        }
    }
}

class Allien extends PlateformeJoueur {

    Allien(int Xposition, int Yposition, int col1, int col2, int col3) {
        x = Xposition;
        y = Yposition;
        coul1 = col1;
        coul2 = col2;
        coul3 = col3;
        Vaisseau = new String[5];
        Vaisseau[0] = "1111111";
        Vaisseau[1] = "1001001";
        Vaisseau[2] = "1111111";
        Vaisseau[3] = "1100011";
        Vaisseau[4] = "1111111";
    }

    void Actualise() {
        if (frameCount % 30 == 0) x += direction * Allienbloc;
        if (incy == true) y += Allienbloc / 2;
    }

    boolean vivant() {
        for (int i = 0; i < projectile.size(); i++) {
            Projectile missile = (Projectile) projectile.get(i);
            if (missile.x > x && missile.x < x + 7 * Taille + 5 && missile.y > y && missile.y < y + 5 *
                    Taille) {
                projectile.remove(i);
                return false;
            }
        }
        return true;
    }

    boolean dehors() {
        if (x + (direction * Allienbloc) < 0 || x + (direction * Allienbloc) > width - Allienbloc) {
            return true;
        } else {
            return false;
        }
    }
}

class Projectile {
    int x, y;

    Projectile(int Xposition, int Yposition) {
        x = Xposition + Allienbloc / 2 - 4;
        y = Yposition;
    }

    void draw() {
        rect(x, y, 5, 30);
        y -= Taille;
    }
}